<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function Category()
    {
        return $this->belongsTo(Categorie::class, 'categorie_id');
    }
    public function Order()
    {
        return $this->hasMany(Order::class);
    }
    public function Cart()
    {
        return $this->belongsToMany(Cart::class)->withPivot('quantity');
    }
    public static function search($keyword)
    {
        return self::where('name', 'like', "%$keyword%")
            ->orWhere('description', 'like', "%$keyword%")
            ->orWhereHas('category', function ($categoryQuery) use ($keyword) {
                $categoryQuery->where('name', 'like', "%$keyword%");
            })
            ->get();
    }
}
