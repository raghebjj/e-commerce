<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public $searchResults = [];
    public function searchProducts($keyword)
    {
        $results = Product::search($keyword);

        // Do something with the search results, such as returning them in a response
        return response()->json($results);
    }
    // public function testwifi()
    // {
    //     $output = shell_exec('C:\\Windows\\System32\\netsh wlan show interfaces');
    //     return $output1 = strtoupper($output->SSID);
    //     $output = shell_exec('C:\\Windows\\System32\\netsh wlan show interfaces 2>&1');
    //     if ($output === null) {
    //         die('Error executing command: ' . error_get_last()['message']);
    //     }
    //     // dd($output);
    //     // Execute system command to get Wi-Fi SSID (Windows example)
    //     // $output = shell_exec('netsh wlan show interfaces');

    //     // Extract SSID from the output using regular expressions
    //     preg_match('/SSID\s*:\s([^\r\n]+)/', $output, $matches);
    //     $wifiSSID = isset($matches[1]) ? trim($matches[1]) : 'Not Available';

    //     $test = (json_encode($output));
    //     (json_decode($test, false));
    //     return $test;
    //     return view('wifi-network', ['wifiSSID' => $wifiSSID]);
    // }
    public function testsearch($searchQuery)
    {
        $this->searchResults = Product::with('category')
            ->where('name', 'like', "%$searchQuery%")
            ->orWhereHas('category', function ($categoryQuery) use ($searchQuery) {
                $categoryQuery->where('name', 'like', "%$searchQuery%");
            })
            ->get();

            return $this->searchResults;
    }
}
