<?php

namespace App\Livewire\Categories;

use App\Models\Categorie;
use Livewire\Component;

class ShowCategories extends Component
{
    public $categories;
   public function mount()
    {
        $this->categories = Categorie::orderBy('name')->get();
    }
    public function render()
    {
        return view('livewire.categories.show-categories');
    }
}
