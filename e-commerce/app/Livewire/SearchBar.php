<?php

namespace App\Livewire;

use App\Models\Product;
use Livewire\Component;

class SearchBar extends Component
{
    public $search;
    public $searchResults = [];

    public function render()
    {
        $this->searchResults = Product::where('name', 'like', '%' . $this->search . '%')
            ->orWhere('description', 'like', '%' . $this->search . '%')
            // ->orWhereHas('category', function ($categoryQuery) use ($search) {
            //     $categoryQuery->where('name', 'like', '%' . $this->search);
            // })
            ->get();
        // $this->searchResults = Product::search($this->search)->get();

        return view('livewire.search-bar');
    }

    public function search($searchQuery)
    {
        // $this->searchResults = Product::where('name', 'like', "%$this->search%")
        //     ->orWhere('category', 'like', "%$this->search%")
        //     ->get();
        $this->searchResults = Product::with('category')
            ->where('name', 'like', "%$searchQuery%")
            ->orWhereHas('category', function ($categoryQuery) use ($searchQuery) {
                $categoryQuery->where('name', 'like', "%$searchQuery%");
            })->take(5)
            ->get();
        // $this->emit('searchResults', $this->searchResults);
    }
    // public function search()
    // {
    //     $this->searchResults = Product::search($this->search)->get();
    // }
}
