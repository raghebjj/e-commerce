<?php

namespace App\Livewire;

use App\Models\Categorie;
use App\Models\Product;
use Livewire\Component;

class CategoryProducts extends Component
{
    public $category;
    public $products;
    public $categories;

    public function mount($category)
    {
        // Fetch products by category from the database
        $this->categories = Categorie::all(); // Retrieve your categories from the database
        $this->category = Categorie::find($category);
        $this->products = Product::where('categorie_id', $category)->get();
    }
    public function render()
    {
        return view(
            'livewire.category-products',
            ['products' => $this->products]
            // ['category' => $this->category, 'categories' => $this->categories]
        );
    }
}
