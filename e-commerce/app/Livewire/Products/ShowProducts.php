<?php

namespace App\Livewire\Products;

use App\Models\Product;
use Livewire\Component;
use Livewire\WithPagination;

class ShowProducts extends Component
{
    use WithPagination;
    public $products;
    public $searchKeyword;
    public function mount()
    {
        // Fetch products from the database
        $this->products = Product::latest()->get();
    }
    public function render()
    {
        // return view('livewire.products.show-products', ($this->products));
        return view('livewire.products.show-products', [
            'products' => $this->searchKeyword
                ? Product::search($this->searchKeyword)
                : Product::latest()->get(),
        ]);
    }
}
