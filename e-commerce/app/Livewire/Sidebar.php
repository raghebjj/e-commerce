<?php

namespace App\Livewire;

use App\Models\Categorie;
use Livewire\Component;

class Sidebar extends Component
{
    public $categories;
    public function mount()
    {
        $this->categories = Categorie::all();

    }
    public function render()
    {
        return view('livewire.sidebar');
    }
}
