<?php

namespace Database\Factories;

use App\Models\Categorie;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Categorie>
 */
class CategorieFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $categories = [
            [
                'name' => 'Electronics',
                'description' => 'Explore cutting-edge gadgets and devices in the world of electronics. From the latest smartphones to smart home technologies, discover innovation at your fingertips.',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Apparel & Fashion',
                'description' => "Stay ahead of the fashion curve with our trendy and stylish clothing collection. Whether it's casual wear or high-end fashion, express your unique style effortlessly.",
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Home & Kitchen',
                'description' => "Transform your living space with functional and decorative items for your home and kitchen. Find everything from cozy blankets to sleek kitchen gadgets for a modern and comfortable home.",
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Sports & Outdoors',
                'description' => "Gear up for adventure with our collection of equipment and gear for various sports and outdoor activities. Whether you're a fitness enthusiast or an outdoor explorer, we've got you covered.",
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Health & Beauty',
                'description' => "Enhance your well-being and beauty with our curated selection of health and beauty products. From skincare essentials to fitness gear, prioritize self-care with our quality offerings.",
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Toys & Games',
                'description' => "Foster creativity and joy with our range of fun and educational toys for children of all ages. Explore a world of imagination and play with our diverse toy collection.",
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Furniture & Decor',
                'description' => "Elevate your living space with quality furniture and decor items. Discover a range of stylish and functional pieces to add a touch of elegance to your home.",
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Books & Stationery',
                'description' => "Immerse yourself in the world of literature and creativity. Our extensive collection of books and stationery offers something for everyone, from thrilling novels to artistic stationery supplies.",
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Automotive',
                'description' => "Dive into the world of automotive excellence with our range of parts, accessories, and tools. Whether you're a car enthusiast or a DIY mechanic, find everything you need for your vehicle.",
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Jewelry & Accessories',
                'description' => "Make a statement with our elegant and stylish jewelry pieces and accessories. From classic pieces to trendy accessories, find the perfect finishing touches for every occasion.",
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ];
        foreach ($categories as $category) {
            Categorie::create($category);
        }
    }
}
