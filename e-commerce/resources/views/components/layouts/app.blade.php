<!-- resources/views/components/layouts/app.blade.php -->

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Laravel') }}</title>
</head>

<body>
    <div class="flex">
        <div class="w-1/4 bg-gray-200 p-4">
            <h3 class="text-lg font-semibold mb-4">Categories</h3>
            {{-- Include a check if $category is set --}}
            @isset($category)
                @if ($category->products->count() == 0)
                    <h1>
                        there is no Products yet!
                    </h1>
                @endif
                @foreach ($category->products as $product)
                    <div class="mb-4">
                        <img src="{{ $product->image_path }}" alt="{{ $product->name }}"
                            class="w-full h-10 object-cover rounded-md mb-2">
                        <p class="text-sm text-gray-500"> {{ optional($product->category)->name }}
                        </p>
                        <h3 class="text-lg font-semibold">{{ $product->name }}</h3>
                        <p class="text-gray-600">{{ $product->description }}</p>
                        <p class="text-gray-600">{{ $product->price }}$</p>
                    </div>
                @endforeach
            @endisset
        </div>
        <div class="w-3/4 p-4">
            <a href="{{ url()->previous() }}" class="back-button">Back</a>
            @yield('content')
        </div>
    </div>
</body>
<style>
    .back-button {
        background-color: purple;
        color: white;
        padding: 10px;
        border: none;
        border-radius: 5px;
        cursor: pointer;
    }

    .back-button:hover {
        background-color: darkslateblue;
    }
</style>

</html>
