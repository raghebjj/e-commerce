<!-- resources/views/welcome.blade.php -->
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Alibaba</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    @livewireStyles

</head>

<body class="antialiased">
    <div
        class="relative sm:flex sm:justify-center sm:items-center min-h-screen bg-dots-darker bg-center bg-gray-100 dark:bg-dots-lighter dark:bg-gray-900 selection:bg-red-500 selection:text-white">

        @if (Route::has('login'))
            <livewire:welcome.navigation />
        @endif
        <div class="sm:flex">
            <livewire:sidebar />
            <div class="sm:w-3/4 p-4">
                <button wire:click="searchProducts" class="bg-blue-500 text-white p-2 rounded-md">Search</button>

                @livewire('products.show-products')
            </div>

        </div>
        <livewire:scripts />

        {{-- <script>
            function searchProducts() {
                Livewire.emit('search', keyword);
            }
        </script> --}}
</body>

</html>
