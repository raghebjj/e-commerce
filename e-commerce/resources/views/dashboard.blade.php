<x-app-layout>

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>User Dashboard</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                background-color: #f4f4f4;
                margin: 0;
                padding: 0;
            }

            header {
                background-color: #333;
                color: #fff;
                text-align: center;
                padding: 10px;
            }

            .dashboard {
                max-width: 800px;
                margin: 20px auto;
                padding: 20px;
                background-color: #fff;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            }

            h2 {
                color: #333;
            }

            .product {
                border: 1px solid #ddd;
                padding: 10px;
                margin-bottom: 10px;
                background-color: #f9f9f9;
            }

            .category {
                font-style: italic;
                color: #777;
            }
        </style>
    </head>

    <body>

        <header>
            <h1>User Dashboard</h1>
        </header>

        <div class="dashboard">
            <h2>Purchased Products</h2>

            <div class="product">
                <h3>Product 1</h3>
                <p class="category">Category: Electronics</p>
            </div>

            <div class="product">
                <h3>Product 2</h3>
                <p class="category">Category: Clothing</p>
            </div>

            <div class="product">
                <h3>Product 3</h3>
                <p class="category">Category: Books</p>
            </div>

            <!-- Add more products and categories as needed -->

        </div>

    </body>
</x-app-layout>
