<div class="mb-4 col-lg-3 space-y-2 divide-y divide-solid">
    <h1 class="mt-4 text-3xl">Filters</h1>

    <div class="sm:w-1/4 bg-gray-200 p-4">
        <h3 class="text-lg font-semibold mb-4">Categories</h3>
        @foreach ($categories as $category)
            <div>
                <a href="{{ route('category.products', $category) }}"
                    class="text-blue-500 hover:text-blue-700">{{ $category->name }}</a>
            </div>
        @endforeach
    </div>


</div>
