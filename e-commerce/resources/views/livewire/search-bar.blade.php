<!-- resources/views/livewire/search-bar.blade.php -->
<!DOCTYPE html>
<html>

<head>
    @livewireStyles

</head>

<body>
    {{-- <div class="search-container">
        <div class="search-bar">
            <input type="text" id="search-input" class="search-input" placeholder="Search">
            <div id="search-results" class="search-results"></div>
            <div class="search-button" onclick="submitSearch()"><i class="fa fa-refresh fa-spin"></i></div>
        </div>
    </div> --}}
    <div class="search-container">
        <div class="search-bar">
            <input type="text" wire:model.debounce.300ms="search" class="search-input" placeholder="Search">
            <div id="search-results" class="search-results">
                @if ($searchResults)
                    @foreach ($searchResults as $result)
                        <div class="search-result">{{ $result->name }}</div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    <div>
        <input wire:model="search" type="text" placeholder="Search..." class="search-input">
        <div class="search-results">
            @foreach ($searchResults as $result)
                <div class="search-result" wire:click="selectResult('{{ $result->id }}')">
                    {{ $result->name }}
                </div>
            @endforeach
        </div>
        <button wire:click="search">Search</button>
    </div>
    {{-- <div class="search-container">
        <div class="search-bar">
            <input type="text" id="search-input" wire:model="search" class="search-input" placeholder="Search"
                wire:input="search">
            <div id="search-results" class="search-results">
                @if ($searchResults)
                    @foreach ($searchResults as $result)
                        <div class="search-result">{{ $result->name }}</div>
                    @endforeach
                @endif
            </div>
            <div class="search-button" wire:click="search"><i class="fa fa-refresh fa-spin"></i></div>
        </div>
    </div> --}}
    {{-- @livewireScripts --}}
</body>
<style>
    .search-container {
        display: flex;
        justify-content: center;
        margin-top: 20px;
    }

    .search-bar {
        position: relative;
        width: 300px;
        height: 30px;
    }

    .search-input {
        width: 100%;
        height: 100%;
        padding: 5px 10px;
        border: 1px solid #ccc;
        border-radius: 5px;
        outline: #000;
    }

    .search-button {
        position: absolute;
        top: 0;
        right: 0;
        width: 30px;
        height: 30px;
        background-color: #000;
        color: #4f2f2f;
        border-radius: 50%;
        display: flex;
        justify-content: center;
        align-items: center;
        cursor: pointer;
    }

    .search-results {
        position: absolute;
        top: 100%;
        width: 100%;
        max-height: 150px;
        overflow-y: auto;
        border: 1px solid #ccc;
        border-radius: 5px;
        background-color: #fff;
        z-index: 1;
    }

    .search-result {
        padding: 5px 10px;
        cursor: pointer;
    }
</style>
<script>
    document.addEventListener('livewire:load', function() {
        const searchInput = document.getElementById("search-input");
        const searchResults = document.getElementById("search-results");

        searchInput.addEventListener("input", search);

        function search() {
            const searchQuery = searchInput.value.toLowerCase();
            const matchedProducts = products.filter(product => product.toLowerCase().includes(searchQuery));
            renderResults(matchedProducts);
        }

        function renderResults(matchedProducts) {
            searchResults.innerHTML = "";
            matchedProducts.forEach(product => {
                const resultItem = document.createElement("div");
                resultItem.classList.add("search-result");
                resultItem.textContent = product;
                searchResults.appendChild(resultItem);
            });
        }
    });
</script>
{{-- <script>
    const searchInput = document.getElementById("search-input");
    const searchResults = document.getElementById("search-results");

    searchInput.addEventListener("input", search);

    function search() {
        const searchQuery = searchInput.value.toLowerCase();
        Livewire.emit('search', searchQuery);
    }

    Livewire.on('searchResults', (matchedProducts) => {
        renderResults(matchedProducts);
    });

    function renderResults(matchedProducts) {
        searchResults.innerHTML = "";
        matchedProducts.forEach(product => {
            const resultItem = document.createElement("div");
            resultItem.classList.add("search-result");
            resultItem.textContent = product.name; // Assuming the product object has a 'name' property
            searchResults.appendChild(resultItem);
        });
    }
</script> --}}

{{-- <script>
    const products = [
        "Product 1",
        "Product 2",
        "Product 3",
        "Product 4",
        "Product 5"
    ];

    const searchInput = document.getElementById("search-input");
    const searchResults = document.getElementById("search-results");

    searchInput.addEventListener("input", search);

    function search() {
        const searchQuery = searchInput.value.toLowerCase();
        const matchedProducts = products.filter(product => product.toLowerCase().includes(searchQuery));
        renderResults(matchedProducts);
    }

    function renderResults(matchedProducts) {
        searchResults.innerHTML = "";
        matchedProducts.forEach(product => {
            const resultItem = document.createElement("div");
            resultItem.classList.add("search-result");
            resultItem.textContent = product;
            searchResults.appendChild(resultItem);
        });
    }
</script> --}}

</html>
