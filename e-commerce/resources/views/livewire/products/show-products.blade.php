<!-- resources/views/livewire/products/show-products.blade.php -->
<section class="container mx-auto my-8">
    <h2 class="text-3xl font-semibold mb-4">Products</h2>
    <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-8">
        @foreach ($products as $product)
            <div class="bg-white p-4 shadow-md rounded-md">
                <div class="mb-4">
                    <img src="{{ $product->image_path }}" alt="{{ $product->name }}"
                        class="w-full h-10 object-cover rounded-md">
                </div>
                <div>
                    <p class="text-sm text-gray-500">{{ optional($product->category)->name }}</p>
                    <h3 class="text-lg font-semibold">{{ $product->name }}</h3>
                    <p class="text-gray-600">{{ $product->description }}</p>
                    <p class="text-gray-600">{{ $product->price }}$</p>
                </div>
            </div>
        @endforeach

    </div>
</section>
