<?php

new class extends Component {
    public function show($id)
    {
        $product = Product::find($id);
        if ($product) {
            return $this->json(['product' => $product], 200);
        } else {
            return $this->json(['product' => 'product doesn"t exist'], 200);
        }
    }
};
