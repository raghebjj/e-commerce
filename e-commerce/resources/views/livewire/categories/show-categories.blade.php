<!-- resources/views/livewire/categories/show-categories.blade.php -->

<div>
    <div class="sm:w-1/4 bg-gray-200 p-4">
        <h3 class="text-lg font-semibold mb-4">Categories</h3>
        @foreach ($categories as $category)
            <div>
                <h2 class="text-blue-500 hover:text-blue-700">{{ $category->name }}</h2>
            </div>
        @endforeach
    </div>
</div>
