<!-- resources/views/livewire/products/category-products.blade.php -->
@extends('components.layouts.app')

@section('content')
    <section class="flex">
        <div class="w-1/4 bg-gray-200 p-4">
            <h2>Products in {{ $category->name }}</h2>
            <div class="flex flex-wrap">
                @foreach ($category->products as $product)
                    <div class="w-1/2 mb-4 px-2">
                        <img src="{{ $product->image_path }}" alt="{{ $product->name }}"
                            class="w-full h-10 object-cover rounded-md mb-2">
                        <p class="text-sm text-gray-500"> {{ optional($product->category)->name }} </p>
                        <h3 class="text-lg font-semibold">{{ $product->name }}</h3>
                        <p class="text-gray-600">{{ $product->description }}</p>
                        <p class="text-gray-600">{{ $product->price }}$</p>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
