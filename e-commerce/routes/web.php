<?php

use App\Http\Controllers\ProductController;
use App\Livewire\CategoryProducts;
use App\Livewire\Products\ShowProducts;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::view('/', 'welcome')->name('homepage');

Route::view('dashboard', 'dashboard')
    ->middleware(['auth', 'verified'])
    ->name('dashboard');

Route::view('profile', 'profile')
    ->middleware(['auth'])
    ->name('profile');
Route::get('/products/{productId}', ShowProducts::class);

Route::view('/products', 'products')->name('products');
Route::get('/categories/{category}', CategoryProducts::class)->name('category.products');

Route::get('/wifi-network', [ProductController::class, 'testwifi']);

require __DIR__ . '/auth.php';
